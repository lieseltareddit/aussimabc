<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\HomePageFeature;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $articles = Article::all()->sortByDesc('id')->take(20);
        $features = HomePageFeature::all()->sortByDesc('id')->take(10);
        return view('index', compact('articles', 'features'));
    }

    public function category($category)
    {
        $category = ArticleCategory::where('slug', $category)->firstOrFail();
        $articles = Article::where('category_id', $category->id)->get()->sortByDesc('id')->take(20);
        return view('category', compact('category', 'articles'));
    }

    public function articleView($date, $article)
    {
        $article = Article::where('date', $date)->where('slug', $article)->firstOrFail();
        $category = ArticleCategory::whereId($article->category_id)->firstOrFail();
        if ($article->category != $category) { abort(404); }
        $article->views++;
        $article->save();
        return view('article', compact('article', 'category'));
    }
}
