<?php

namespace App\Http\Controllers;

use App\ByElectionCandidate;
use App\FederalElection;
use Illuminate\Http\Request;
use App\ByElection;

class ElectionsController extends Controller
{
    public function index()
    {
        $byelections = ByElection::all()->sortByDesc('polling_day');
        $fedelections = FederalElection::all()->sortByDesc('polling_day');
        return view('elections.index', compact('byelections', 'fedelections'));
    }

    public function byElectionIndex($slug)
    {
        $election = ByElection::where('slug', $slug)->firstOrFail();
        $candidates = ByElectionCandidate::where('by_election_id', $election->id)->get();
        $sortedCandidates = ByElectionCandidate::where('by_election_id', $election->id)->get()->sortByDesc('first_preference_votes');
        $winning = ByElectionCandidate::whereId($election->winning_candidate)->firstOrFail();
        $tppcandidates = ByElectionCandidate::where('by_election_id', $election->id)->where('total_votes', '!=', 0)->get();
        return view('elections.byelection.index', compact('election', 'sortedCandidates', 'candidates', 'tppcandidates', 'winning'));
    }

    public function federalElectionIndex($month, $year, $id)
    {
        $election = FederalElection::whereId($id)->firstOrFail();
        return view('elections.federal.index', compact('election'));
    }
}
