@extends('layouts.main')

@section('content')
    <!-- A modules - start -->
    <!-- A modules - end -->
    <div class="page section">
    
        <div class="subcolumns">
            <div class="c75l">
                <!-- B modules - start -->
                <div class="subcolumns ">
                    <!-- First Half: 66 modules - start -->
                    <div class="c66l">
                        {{-- <a href="/news/australia-votes">
                        <img src="https://cdn.discordapp.com/attachments/422359065209995275/587101693792223244/unknown.png" style="width: 100%;">
                        </a> --}}
                        <div class="inline-content uberlist ng3ts avoid-orphans copyfit-image-ratio dividers default-image-width-160 heading-labels show-teaser-related-doctypes transformed">

                            <div class="section module-body">
                                <ol>
                                @foreach($articles as $article)
                                    <!-- start articles -->
                                        <li class="doctype-article image-half heading-big image-220w">
                                            <h3>
                                                <a href="{{route('news.article', ['date' => $article->date, 'article' => $article->slug])}}">{{$article->title}}</a>
                                            </h3>
                                            <a class="image" aria-hidden="true" tabindex="-1"
                                               href="{{route('news.article', ['date' => $article->date, 'article' => $article->slug])}}">
                                                <img alt="" src="{{$article->image_url}}"
                                                     data-src="{{$article->image_url}}" style="opacity: 1;" width="220"
                                                     height="124">
                                            </a>
                                            <div class="text-container"><p>{{$article->caption}}</p></div>
                                        </li>
                                    @endforeach
                                </ol>

                            </div>

                            <!-- First Half: 66 modules - end -->
                        </div>
                    </div>
                    <div class="c33r">
                        <div class="inline-content uberlist uberlist-features copyfit boxed-card boxed-card-black teaser-text-small append-local-item heading-labels transformed">
                            <div class="section module-heading">
                                <h2>Features</h2>
                            </div>
                            <div class="section module-body">
                                <ol>
                                    @foreach ($features as $f)
                                    <li class="doctype-article uberlist-has-kicker" data-importance="5"
                                        data-first-published="2019-06-02T04:58+1000"
                                        data-last-published="2019-06-02T07:58+1000">
                                        <a href="{{$f->url}}">
                                            <span class="image" aria-hidden="fase">
                                            <img alt="" src="{{$f->image_url}}" data-src="{{$f->image_url}}"
                                                 style="opacity: 1;" width="220" height="124">
                                            </span>
                                            @if ($f->label)
                                            <span class="label-kicker">{{$f->label }}<span
                                                        class="accessibility">: </span></span>
                                            @endif
                                            <h3>{{$f->text}}</h3>
                                        </a>
                                    </li>
                                    @endforeach
                                </ol>
                            </div>

                        </div>
                        <!-- Second Half: 33 modules - end -->
                    </div>
                </div>
                <!-- B modules - end -->
            </div>
        </div>
    </div>
@endsection