@extends ('layouts.noheader')

@section('content')
    <link rel="stylesheet" type="text/css"
          href="https://www.abc.net.au/res/sites/news-projects/news-core/1.21.2/desktop.css">
    <link rel="stylesheet"
          href="https://www.abc.net.au/res/sites/news-projects/elections-results-common-responsive/dev-federal-2016/styles/election-desktop.css?6"/>
    <link rel="stylesheet" type="text/css" href="https://www.abc.net.au//cm/code/7342150/Microsite+v2.css">
    <style>
        .platform-standard #header .brand a::before {
            content: 'Federal Election {{$election->month}} {{$election->year}}'
        }
    </style>
    <div id="container_header">
        <div class="page_margins">
            <div id="header" class="header">
                <div class="brand">
                    <a href="/news/federal-election-2016/"><img class="print"
                                                                src="/cm/lb/8212706/data/news-logo-2017---desktop-print-data.png"
                                                                alt="ABC News" width="387" height="100">
                        <img class="noprint" src="/cm/lb/8212704/data/news-logo2017-data.png" alt="ABC News" width="242"
                             height="80">
                    </a></div>
                <a href="/news/federal-election-2016/" class="australia-votes">Australia Votes</a></div>
        </div>
    </div>
    <div id="container_nav">
        <div class="page_margins">
            <div id="nav" class="nav" role="navigation">
                <ul id="primary-nav">
                    <li id="n-news" class=""><a href="/news/">News Home</a></li>
                    <li id="n-news-root-redirect" class="active"><a href="/news/federal-election-2016/">Home</a></li>
                    <li id="n-results-dropdown" class="dropdown">


                        <a href="/news/federal-election-2016/results/">


                            <span>Results</span>


                        </a>
                        <div class="drop">
                            <ul>
                                <li class="first"><a href="/news/federal-election-2016/results/">Party Totals</a></li>
                                <li><a href="/news/federal-election-2016/results/list/">Electorate List</a></li>
                                <li><a href="/news/federal-election-2016/results/pendulum/">Pendulum</a></li>
                                <li><a href="/news/federal-election-2016/results/senate/">Senate Results</a></li>
                                <li class="last"><a href="/news/federal-election-2016/map/">Interactive Map</a></li>
                            </ul>
                        </div>
                    </li>
                    <li id="n-guide-dropdown" class="dropdown">


                        <a href="/news/federal-election-2016/guide/">


                            <span>Guide</span>


                        </a>
                        <div class="drop">
                            <ul>
                                <li class="first"><a href="/news/federal-election-2016/guide/">Election Guide</a></li>
                                <li>


                                    <a href="/news/federal-election-2016/guide/preview-national/">


                                        <span>Election Preview</span>


                                    </a>
                                </li>
                                <li><a href="/news/federal-election-2016/guide/candidates/">Candidates A-Z</a></li>
                                <li><a href="/news/federal-election-2016/guide/electorates/">Electorates A-Z</a></li>
                                <li><a href="/news/federal-election-2016/guide/electorates-by-state/">Electorates by
                                        State</a></li>
                                <li><a href="/news/federal-election-2016/guide/departingmps/">Retiring MPs</a></li>
                                <li><a href="/news/federal-election-2016/guide/key-seats/">Key Seats</a></li>
                                <li><a href="/news/federal-election-2016/guide/pendulum/">Pendulum</a></li>
                                <li><a href="/news/federal-election-2016/guide/calculator/">House of Reps Calculator</a>
                                </li>
                                <li><a href="/news/federal-election-2016/guide/senate/">Senate</a></li>
                                <li class="last"><a href="/news/federal-election-2016/map/">Interactive Map</a></li>
                            </ul>
                        </div>
                    </li>
                    <li id="n-vote-compass-redirect" class=""><a href="https://votecompass.abc.net.au">Vote Compass</a>
                    </li>
                    <li id="n-policies-redirect" class=""><a
                                href="/news/2016-05-25/key-issues-where-the-parties-differ/7421638">Policies</a></li>
                    <li id="n-opinion-polls" class=""><a href="/news/federal-election-2016/opinion-polls/">Polls</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="page_margins" id="main_content">
        <div class="subcolumns">
            <div class="c75l">
                <!-- B modules - start -->
                <div class="section">
                    <style>
                        .meter { 
                            height: 35px;  /* Can be anything */
                            position: relative;
                            background: #EEEEEE;
                            box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
                        }

                        .meter > span {
                            display: block;
                            height: 100%;
                            background-image: linear-gradient(
                                center bottom,
                                rgb(43,194,83) 37%,
                                rgb(84,240,84) 69%
                            );
                            position: relative;
                            overflow: hidden;

                            font-size: 1.5em;
                        }

                        #parties {
                            margin-top: 10px;
                        }
                        
                        #parties > .meter {
                            margin-top: 5px;
                            margin-bottom: 5px;
                        }
                        
                        .socialist {
                            background: red;
                            color: white;
                        }

                        .liberal {
                            background: blue;
                            color: white;
                        }

                        .udp {
                            background: teal;
                            color: white;
                        }
                    </style>
                    <h2>State of the parties</h2>
                    <b>
                        @php
                        echo($election->counted_votes / $election->electors * 100);
                        @endphp
                        % COUNTED ({{$election->last_update}}).
                    </b>
                    0 in doubt.
                    <div id="parties">
                        <div class="meter">
                            <span class="socialist" style="width: 60%">
                                <span style="padding-left: 10px; line-height: 35px;">Socialist&nbsp;|&nbsp;6 seats</span>
                            </span>
                        </div>
                        <div class="meter">
                            <span class="liberal" style="width: 33.3%">
                                <span style="padding-left: 10px; line-height: 35px;">Liberal&nbsp;|&nbsp;4 seats</span>
                            </span>
                        </div>
                        <div class="meter">
                            <span class="udp" style="width: 6.6%">
                                <span style="padding-left: 10px; line-height: 35px;">UDP&nbsp;|&nbsp;1 seats</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection