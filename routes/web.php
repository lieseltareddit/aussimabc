<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return redirect()->route('news.index');
});

Route::prefix('news')->group(function () {
   Route::get('/', 'PageController@index')->name('news.index');
    Route::get('{category_id}/{article_id}', function ($category_id, $article_id) {
        abort(301, 'This article has moved URLs. Please find it using the search function.');
    })->where('category_id', '[0-9]+')->where('article_id', '[0-9]+');
   Route::get('{date}/{article}', 'PageController@articleView')->name('news.article');
   Route::get('{category}', 'PageController@category')->name('news.category');
});



Route::get('/elections', 'ElectionsController@index')->name('elections.index');
Route::get('/elections/byelection/{slug}', 'ElectionsController@byElectionIndex')->name('elections.byelection.index');
Route::get('/elections/byelection/{slug}/json', function($slug) {
    $be = \App\ByElection::where('slug', $slug)->firstOrFail();
    return $be->toJson(JSON_PRETTY_PRINT);
});
Route::get('/elections/byelection/{slug}/json/candidates', function($slug) {
    $be = \App\ByElection::where('slug', $slug)->firstOrFail();
    $c = \App\ByElectionCandidate::where('by_election_id', $be->id)->get();
    return $c->toJson(JSON_PRETTY_PRINT);
});



Auth::routes(['register' => false]);

Route::get('/admin/slug/all', function () {
    \App\Article::get()->each(function ($a) {
       $a->slug = \Illuminate\Support\Str::slug($a->title);
       $a->save();
    });

    \App\ArticleCategory::get()->each(function ($c) {
        $c->slug = \Illuminate\Support\Str::slug($c->title);
        $c->save();
    });

    \App\ByElection::get()->each(function ($c) {
        $c->slug = \Illuminate\Support\Str::slug($c->title);
        $c->save();
    });

    \App\FederalElection::get()->each(function ($c) {
        $c->slug = \Illuminate\Support\Str::slug($c->title);
        $c->save();
    });

    \App\Electorate::get()->each(function ($c) {
        $c->slug = \Illuminate\Support\Str::slug($c->title);
        $c->save();
    });
});

//Admin routes
Route::get('/admin', 'AdminController@index')->name('admin.index');
Route::post('/admin/category/add', 'AdminController@addCategory')->name('admin.category.add');
Route::get('/admin/article/view/{id}', 'AdminController@viewArticle')->name('admin.article.view');
Route::post('/admin/article/view/{id}', 'AdminController@editArticle')->name('admin.article.edit');
Route::view('/admin/article/write', 'admin.writearticle')->name('admin.article.write');
Route::post('/admin/article/write', 'AdminController@publishArticle')->name('admin.article.publish');
Route::get('/admin/article/{id}/json', 'AdminController@articleJson')->name('admin.article.json');
Route::get('/admin/article/{id}/pdf', 'AdminController@articlePdf')->name('admin.article.pdf');
